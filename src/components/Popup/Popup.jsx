import styles from './Modal.module.scss';
import { MdClear } from 'react-icons/md';

const Modal = ({ header, text, isCloseButton, closeHandler, actions }) => {
  const closeModalOutSide = (e) => {
    if (e.target.id === 'overlayPopup') {
      closeHandler();
    }
  };
  return (
    <div id='overlayPopup' className={styles.popupOverlay} onClick={closeModalOutSide}>
      <div className={styles.popup}>
        <div className={styles.popupHeader}>
          {header}
          {isCloseButton && (
            <MdClear className={styles.closeIcon} onClick={closeHandler} />
          )}
        </div>
        <div className={styles.popupBody}>{text}</div>
        <div className={styles.wrapperBtns}>{actions}</div>
      </div>
    </div>
  );
};

export default Modal;
