const ActionBtns = ({ firstBtnName = 'Ok', secondBtnName = 'Cancel'}) => {
  return (
    <>
      <button>{firstBtnName}</button>
      <button>{secondBtnName}</button>
    </>
  );
};

export default ActionBtns