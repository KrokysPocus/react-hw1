import { useState } from 'react';
import './App.scss';
import Button from './components/Buttons/Button';
import Modal from './components/Popup/Popup';
import ActionBtns from './components/Buttons/ActionBtns/ActionBtns';

function App() {
  let [firstModalPopup, setFirstModalPopup] = useState(false);
  let [secondModalPopup, setSecondModalPopup] = useState(false);

  const onFirstModalHandler = () => {
    setFirstModalPopup((prev) => (prev = !prev));
    setSecondModalPopup(false);
  };

  const onSecondModalHandler = () => {
    setSecondModalPopup((prev) => (prev = !prev));
    setFirstModalPopup(false);
  };

  const closeHandler = () => {
    setFirstModalPopup(false);
    setSecondModalPopup(false);
  };
  

  return (
    <div className="App">
      <Button
        backgroundColor="#5d2fb0"
        text="Open first modal"
        onModalHandler={onFirstModalHandler}
      />
      {firstModalPopup && (
        <Modal
          header="Do you want to delete this file?"
          text="Once you delete this file, it won’t be possible to undo this action. 
                Are you sure you want to delete it?"
          isCloseButton
          closeHandler={closeHandler}
          actions={<ActionBtns />}
        />
      )}
      <Button
        backgroundColor="#ff9800"
        text="Open second modal"
        onModalHandler={onSecondModalHandler}
      />
      {secondModalPopup && (
        <Modal
          header="Some header can be here"
          text="Description for your Popup can be insert here"
          isCloseButton={false}
          closeHandler={closeHandler}
          actions={<ActionBtns firstBtnName="Apply" secondBtnName='Close'/>}
        />
      )}
    </div>
  );
}

export default App;
